package com.gmail.val59000mc.tooktook.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.tooktook.TookTook;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.game.GameManager;
import com.gmail.val59000mc.tooktook.i18n.I18n;

public class NoLimitModeThread implements Runnable{

	private static NoLimitModeThread instance;
	
	private int remainingTime;
	private boolean run;
	
	
	public static void start(){
		Logger.debug("-> NoLimitModeThread::start");
		Bukkit.getScheduler().runTaskAsynchronously(TookTook.getPlugin(), new NoLimitModeThread());
		Logger.debug("<- NoLimitModeThread::start");
	}
	
	public static void stop(){
		Logger.debug("-> NoLimitModeThread::stop");
		if(instance != null){
			instance.run = false;
		}
		Logger.debug("<- NoLimitModeThread::stop");
	}
	
	public static int getRemainingTime(){
		return (instance != null) ? instance.remainingTime : 0;
	}
	
	public NoLimitModeThread(){
		instance = this;
		this.run = true;
		this.remainingTime = Config.noLimitModeAfter;
	}
	
	public void run() {
		
		if(run){

			
			if(remainingTime > 0){
				if(remainingTime <= 60 && remainingTime%10 == 0){
					Bukkit.getScheduler().runTask(TookTook.getPlugin(), new Runnable() {
						
						public void run() {
							Logger.broadcast(I18n.get("game.no-limit-mode-in").replace("%time%", Time.getFormattedTime(remainingTime+1)));
							Sounds.playAll(Sound.NOTE_PLING);
						}
					});
				}
				remainingTime--;
				Bukkit.getScheduler().runTaskLaterAsynchronously(TookTook.getPlugin(), instance,20);
			}else{
				Bukkit.getScheduler().runTask(TookTook.getPlugin(), new Runnable(){

					public void run() {
						GameManager.instance().startNoLimitMode();
					}
				
				});
			}
			
		}

	}
}