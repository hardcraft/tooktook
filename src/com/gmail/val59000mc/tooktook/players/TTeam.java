package com.gmail.val59000mc.tooktook.players;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.i18n.I18n;

public class TTeam {

	private List<TPlayer> members;
	private TeamType type;
	private Location spawnPoint;
	private int kills;
	
	public TTeam(TeamType type, Location spawnPoint) {
		this.members = new ArrayList<TPlayer>();
		this.type = type;
		this.spawnPoint = spawnPoint;
		this.kills = 0;
	}
	
	public TeamType getType() {
		return type;
	}

	public void setType(TeamType type) {
		this.type = type;
	}


	public void sendI18nMessage(String code) {
		for(TPlayer wicPlayer: members){
			wicPlayer.sendI18nMessage(code);
		}
	}


	public String getI18nName(Player player){
		return I18n.get("team."+getType().toString(), player);
	}
	
	public boolean contains(TPlayer player){
		return members.contains(player);
	}
	
	public synchronized List<TPlayer> getMembers(){
		return members;
	}

	public List<TPlayer> getPlayingMembers(){
		List<TPlayer> playingMembers = new ArrayList<TPlayer>();
		for(TPlayer uhcPlayer : getMembers()){
			if(uhcPlayer.getState().equals(PlayerState.PLAYING)){
				playingMembers.add(uhcPlayer);
			}
		}
		return playingMembers;
	}
	
	public synchronized List<String> getMembersNames(){
		List<String> names = new ArrayList<String>();
		for(TPlayer player : getMembers()){
			names.add(player.getName());
		}
		return names;
	}
	
	public void addKill() {
		this.kills++;
	}
	
	public int getKills() {
		return this.kills;
	}
	
	public void addPlayer(TPlayer gPlayer){
		gPlayer.leaveTeam();
		gPlayer.setTeam(this);
		getMembers().add(gPlayer);
		gPlayer.setSpawnPoint(spawnPoint);
	}
	
	public void leave(TPlayer gPlayer){
		getMembers().remove(gPlayer);
		gPlayer.setSpawnPoint(null);
	}
	
	public boolean isOnline(){
		for(TPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline()){
				return true;
			}
		}
		return false;
	}


	public boolean isPlaying() {
		for(TPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline() && gPlayer.isState(PlayerState.PLAYING)){
				return true;
			}
		}
		return false;
	}
	
	public List<TPlayer> getOtherMembers(TPlayer excludedPlayer){
		List<TPlayer> otherMembers = new ArrayList<TPlayer>();
		for(TPlayer uhcPlayer : getMembers()){
			if(!uhcPlayer.equals(excludedPlayer))
				otherMembers.add(uhcPlayer);
		}
		return otherMembers;
	}

	public Boolean is(TeamType type) {
		return getType().equals(type);
	}

	public ChatColor getColor() {
		return getType().getColor();
	}
	
	public boolean canAddPlayer() {
		if(getMembers().size() < Config.playersToStart/2){
			return true;
		}else{
			return PlayersManager.instance().canAddPlayerToTeam(this);
		}
	}

}
