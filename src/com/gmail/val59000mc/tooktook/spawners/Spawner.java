package com.gmail.val59000mc.tooktook.spawners;

import org.bukkit.Location;

public abstract class Spawner {
	protected Location location;
	protected int limit;
	private int time;
	
	
	
	public Spawner(Location location, int limit, int time) {
		super();
		this.location = location;
		this.limit = limit;
		this.time = time;
	}

	

	public int getTime() {
		return time;
	}



	public abstract void spawn();
}
