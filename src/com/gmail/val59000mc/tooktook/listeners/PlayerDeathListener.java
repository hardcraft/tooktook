package com.gmail.val59000mc.tooktook.listeners;

import java.util.Calendar;
import java.util.Iterator;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.tooktook.TookTook;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.dependencies.VaultManager;
import com.gmail.val59000mc.tooktook.game.GameManager;
import com.gmail.val59000mc.tooktook.game.GameState;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.players.PlayerState;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;

public class PlayerDeathListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(final PlayerDeathEvent event) {

		TPlayer wicPlayer = PlayersManager.instance().getTPlayer(event.getEntity());
		
		if(wicPlayer == null || !wicPlayer.isOnline()){
			return;
		}
		
		handleDeathPlayer(event,wicPlayer);
		
	}
	
	public void handleDeathPlayer(final PlayerDeathEvent event, TPlayer dead){
		PlayersManager pm = PlayersManager.instance();
		
		// Add death score to dead player and kill score to killer if playing
		if(GameManager.instance().isState(GameState.PLAYING)){
			
			TPlayer killer = (dead.getPlayer().getKiller() == null) ? null : pm.getTPlayer(dead.getPlayer().getKiller());
			
			
			int arrows = 0;
			int enderpearls = 0;
			Iterator<ItemStack> iterator = event.getDrops().iterator();
			while(iterator.hasNext()){
				ItemStack drop = iterator.next();
				
				switch(drop.getType()){
					case ARROW:
						arrows += drop.getAmount();
						iterator.remove();
						break;
					case ENDER_PEARL:
						enderpearls += drop.getAmount();
						iterator.remove();
						break;
					case LEATHER_HELMET:
					case LEATHER_CHESTPLATE:
					case WOOD_SWORD:
					case WOOD_AXE:
					case STONE_PICKAXE:
						if(drop.getEnchantments().size() == 0){
							iterator.remove();
						}
						break;
					default:
						break;
				}
			}
			
			
			event.setDeathMessage(I18n.get("player.died").replace("%player%",dead.getColor()+dead.getName()));
			
			dead.addDeath();
			dead.setLastEnderpearl(Calendar.getInstance().getTimeInMillis()-12000);
			
			if(killer != null && killer.isOnline()){
				killer.addKill();
				killer.addMoney(rewardKill(killer.getPlayer()));
				event.setDeathMessage(
						I18n.get("player.killed")
						.replace("%killer%",killer.getColor()+killer.getName())
						.replace("%killed%",dead.getColor()+dead.getName())
				);
				
				int arrowLoots = ((Double) ((double) arrows*killer.getArrowsDropRate()/100)).intValue();
				int enderpearlLoots = ((Double) ((double) enderpearls*killer.getEnderpearlsDropRate()/100)).intValue();
				arrowLoots = ((arrowLoots == 0 && arrows > 0) ? 1 : arrowLoots);
				enderpearlLoots = ((enderpearlLoots == 0 && enderpearls > 0) ? 1 : enderpearlLoots);
				
				if(arrowLoots > 0){ event.getDrops().add(new ItemStack(Material.ARROW, arrowLoots)); }
				if(enderpearls > 0){ event.getDrops().add(new ItemStack(Material.ENDER_PEARL, enderpearlLoots)); }
			}
			
			Bukkit.getScheduler().runTaskLater(TookTook.getPlugin(), new Runnable() {
				
				public void run() {
			        event.getEntity().spigot().respawn();
				}
			}, 20);

		}
		
	}
	
	private double rewardKill(Player killer) {
		double reward = 0;
		if(Config.isVaultLoaded){
			reward = VaultManager.addMoney(killer, Config.killReward);
			killer.sendMessage(ChatColor.GREEN+"+"+reward+" "+ChatColor.WHITE+"HC");
			Sounds.play(killer, Sound.LEVEL_UP, 0.5f, 2f);
		}
		return reward;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Logger.debug("-> PlayerDeathListener::onPlayerRespawn, player="+event.getPlayer().getName());
		
		TPlayer wicPlayer = PlayersManager.instance().getTPlayer(event.getPlayer());
		
		if(wicPlayer == null){
			return;
		}
		
		handleRespawnPlayer(event,wicPlayer);
		
		Logger.debug("<- PlayerDeathListener::onPlayerRespawn");
	}
	
	public void handleRespawnPlayer(PlayerRespawnEvent event, TPlayer evoPlayer){
		Logger.debug("-> PlayerDeathListener::handleRespawnPlayer, player="+evoPlayer);

		final String name = evoPlayer.getName();
		
		switch(GameManager.instance().getState()){
			case WAITING:
					event.setRespawnLocation(Config.lobby);
					Bukkit.getScheduler().runTaskLater(TookTook.getPlugin(), new Runnable() {
					
					public void run() {
						Logger.debug("waitPlayer");
						TPlayer p = PlayersManager.instance().getTPlayer(name);
						if(p != null){
							PlayersManager.instance().waitPlayer(p);
						}
						
					}
				}, 1);
				break;
			case LOADING:
			case STARTING:
			case ENDED:
			default:
			case PLAYING:
				if(evoPlayer.getSpawnPoint() != null){
					event.setRespawnLocation(evoPlayer.getSpawnPoint());
				}
				Bukkit.getScheduler().runTaskLater(TookTook.getPlugin(), new Runnable() {
					
					public void run() {
						TPlayer p = PlayersManager.instance().getTPlayer(name);
						if(p.isState(PlayerState.PLAYING)){
							PlayersManager.instance().teamSpawnPlayer(p);
						}
						
					}
				}, 1);
				break;
		}
		

		Logger.debug("<- PlayerDeathListener::handleRespawnPlayer");
	}
	
}
