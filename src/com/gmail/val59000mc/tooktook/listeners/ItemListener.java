package com.gmail.val59000mc.tooktook.listeners;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;

public class ItemListener implements Listener{
	
	private Map<UUID,Long> lastUseEnderpearl;
		
	
	public ItemListener() {
		this.lastUseEnderpearl = Collections.synchronizedMap(new HashMap<UUID,Long>());
	}
	
	

	public synchronized Map<UUID, Long> getLastUseEnderpearl() {
		return lastUseEnderpearl;
	}



	@EventHandler(priority=EventPriority.HIGHEST)
	public void onUseEnderpearl(final PlayerInteractEvent event){
		TPlayer tPlayer = PlayersManager.instance().getTPlayer(event.getPlayer());
		
		if(tPlayer == null){
			event.setCancelled(true);
			return;
		}
		
		handleUseEnderpearl(event,tPlayer);
	}

	private void handleUseEnderpearl(PlayerInteractEvent event, TPlayer tPlayer) {
		if(tPlayer.isOnline()){
			Player player = tPlayer.getPlayer();
			if( (
					event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || 
					event.getAction().equals(Action.RIGHT_CLICK_AIR)
				) 
				&& player.getItemInHand().getType().equals(Material.ENDER_PEARL)){
				
				Long now = Calendar.getInstance().getTimeInMillis();
				UUID uuid = player.getUniqueId();
				Long lastUse = getLastUseEnderpearl().get(uuid);
				if(lastUse != null && now-lastUse < 15000){
					event.setCancelled(true);
					Logger.sendMessage(player, I18n.get("player.wait-enderpearl", player).replace("%time%",Time.getFormattedTime((now-lastUse)/1000)));
				}else{
					getLastUseEnderpearl().put(uuid, now);
				}
			}
		}		
	}
	
}
