package com.gmail.val59000mc.tooktook.configuration;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.Plugin;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.api.SIGApi;
import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.TriggerParseException;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.tooktook.TookTook;
import com.gmail.val59000mc.tooktook.dependencies.SelectTeamActionParser;
import com.gmail.val59000mc.tooktook.dependencies.TeamPlaceholder;
import com.gmail.val59000mc.tooktook.dependencies.VaultManager;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.players.TeamType;
import com.gmail.val59000mc.tooktook.spawners.EntitySpawner;
import com.gmail.val59000mc.tooktook.spawners.ItemSpawner;
import com.gmail.val59000mc.tooktook.spawners.Spawner;

import net.milkbowl.vault.Vault;

public class Config {
	
	// Dependencies
	public static boolean isVaultLoaded;
	public static boolean isSIGLoaded;
	public static boolean isBountifulApiLoaded;
	
	// World 
	public static String lastWorldName;
	public static String worldName;
	public static boolean isLoadLastWord;

	// Players
	public static int playersToStart;
	public static int maxPlayers;
	public static int countdownToStart;
	public static int killAfterDisconnect;
	
	// Locations
	public static Location lobby;
	public static Location redSpawn;
	public static Location blueSpawn;
	public static LocationBounds bounds;
	public static Location redBeacon;
	public static Location blueBeacon;

	// No Limit
	public static int noLimitModeAfter;
	public static int timerAfterNoLimit;
	public static boolean noLimit;
	
	// Spawner
	public static int enderpearlTime;
	public static int enderpearlLimit;
	public static int cowTime;
	public static int cowLimit;
	public static int snowballTime;
	public static int snowballLimit;
	public static Set<Spawner> spawners;
	
	
	// Bungee
	public static boolean isBungeeEnabled;
	public static String bungeeServer;

	
	// Rewards
	public static double killReward;
	public static double winReward;
	public static double objectiveReward;
	public static double vipRewardBonus;
	public static double vipPlusRewardBonus;
	public static double helperRewardBonus;
	public static double builderRewardBonus;
	public static double moderateurRewardBonus;
	public static double administrateurRewardBonus;
	
	public static void load(){
		
		FileConfiguration cfg = TookTook.getPlugin().getConfig();

		// Debug
		Logger.setDebug(cfg.getBoolean("debug",false));
		
		Logger.debug("-> Config::load");
		
		// World
		lastWorldName = cfg.getString("world.last-world","last_wic_world");
		isLoadLastWord = cfg.getBoolean("world.load-last-world",true);
		worldName = UUID.randomUUID().toString();
		
		// Players
		playersToStart = cfg.getInt("players.min",21);
		countdownToStart = cfg.getInt("players.countdown",10);
		maxPlayers = cfg.getInt("players.max",12);
		killAfterDisconnect = cfg.getInt("players.kill-after-disconnect",300);

		// No Limit Mode
		noLimitModeAfter = cfg.getInt("no-limit-mode-after",1200);
		timerAfterNoLimit = cfg.getInt("timer-after-no-limit",900);
		noLimit = false;
		
		// Spawner
		enderpearlTime = cfg.getInt("spawner.enderpearl-time",300);
		enderpearlLimit = cfg.getInt("spawner.enderpearl-limit",16);
		cowTime = cfg.getInt("spawner.cow-time",600);
		cowLimit = cfg.getInt("spawner.cow-limit",5);
		snowballTime = cfg.getInt("spawner.snowball-time",100);
		snowballLimit = cfg.getInt("spawner.snowball-limit",30);
  
		

		// Bungee
		isBungeeEnabled = cfg.getBoolean("bungee.enable",false);
		bungeeServer = cfg.getString("bungee.server","lobby");
		
		
		// Rewards
		killReward = cfg.getDouble("reward.kill",5);
		winReward = cfg.getDouble("reward.win",50);
		objectiveReward = cfg.getDouble("reward.objective",15);
		vipRewardBonus = cfg.getDouble("reward.vip",100);
		vipPlusRewardBonus = cfg.getDouble("reward.vip+",100);
		helperRewardBonus = cfg.getDouble("reward.helper",100);
		builderRewardBonus = cfg.getDouble("reward.builder",100);
		moderateurRewardBonus = cfg.getDouble("reward.moderateur",100);
		administrateurRewardBonus = cfg.getDouble("reward.administrateur",100);
		
		// Dependencies
		loadVault();
		loadSIG();
		loadBountifulApi();
		
		// Sig
		if(isSIGLoaded){
			
			SIGApi sig = SIG.getPlugin().getAPI();
			sig.registerActionParser(new SelectTeamActionParser());
			sig.registerPlaceholder(new TeamPlaceholder("{members.RED}",TeamType.RED));
			sig.registerPlaceholder(new TeamPlaceholder("{members.BLUE}",TeamType.BLUE));
			
			try {
				sig.registerConfigurationFile(new File(TookTook.getPlugin().getDataFolder(), "teams.yml"));
			} catch (InventoryParseException | TriggerParseException e) {
				//
			}
			
		}
		
		
		VaultManager.setupEconomy();
		Logger.debug("<- Config::load");
	}
	
	private static void loadVault(){
		Logger.debug("-> Config::loadVault");
		
		Plugin vault = Bukkit.getPluginManager().getPlugin("Vault");
        if(vault == null || !(vault instanceof Vault)) {
            Logger.warn(I18n.get("config.dependency.vault-not-found"));
        	 isVaultLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.vault-loaded"));
        	 isVaultLoaded = true;
        }
		Logger.debug("<- Config::loadVault");
	}
	
	private static void loadBountifulApi(){
		Logger.debug("-> Config::loadBountifulApi");
		
		Plugin bountifulApi = Bukkit.getPluginManager().getPlugin("BountifulAPI");
        if(bountifulApi == null || !(bountifulApi instanceof BountifulAPI)) {
            Logger.warn(I18n.get("config.dependency.bountiful-not-found"));
        	 isBountifulApiLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.bountifulapi-loaded"));
            isBountifulApiLoaded = true;
        }
		Logger.debug("<- Config::loadBountifulApi");
	}

	private static void loadSIG(){
		Logger.debug("-> Config::loadSIG");
		
		Plugin sig = Bukkit.getPluginManager().getPlugin("SimpleInventoryGUI");
        if(sig == null || !(sig instanceof SIG)) {
            Logger.warn(I18n.get("config.dependency.sig-not-found"));
        	 isSIGLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.sig-loaded"));
            isSIGLoaded = true;
        }
		Logger.debug("<- Config::loadSIG");
	}
		
	
	public static void loadLocations(){
		World world = Bukkit.getWorld(worldName);
		
		FileConfiguration cfg = TookTook.getPlugin().getConfig();
		
		// Locations
		lobby = Parser.parseLocation(world, cfg.getString("locations.lobby"));
		redSpawn = Parser.parseLocation(world, cfg.getString("locations.red-spawn"));
		blueSpawn = Parser.parseLocation(world, cfg.getString("locations.blue-spawn"));
		bounds = new LocationBounds(Parser.parseLocation(world, cfg.getString("locations.bounds.min")), Parser.parseLocation(world, cfg.getString("locations.bounds.max")));
		redBeacon = Parser.parseLocation(world, cfg.getString("locations.red-beacon"));
		blueBeacon = Parser.parseLocation(world, cfg.getString("locations.blue-beacon"));
		
		

		spawners = new HashSet<Spawner>();
		spawners.add(new ItemSpawner(Parser.parseLocation(world, cfg.getString("locations.snowball")), Material.SNOW_BALL, snowballLimit, snowballTime));
		for(String cowLoc : cfg.getStringList("locations.cow")){
			spawners.add(new EntitySpawner(Parser.parseLocation(world, cowLoc), EntityType.COW, cowLimit, cowTime));
		}
		for(String enderpearlLoc : cfg.getStringList("locations.enderpearl")){
			spawners.add(new ItemSpawner(Parser.parseLocation(world, enderpearlLoc), Material.ENDER_PEARL, enderpearlLimit, enderpearlTime));
		}
	}
		
}
