package com.gmail.val59000mc.tooktook.game;

public enum GameState {
	LOADING,
	WAITING,
	STARTING,
	PLAYING,
	ENDED;
}
