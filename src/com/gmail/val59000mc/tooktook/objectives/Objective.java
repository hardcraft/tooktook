package com.gmail.val59000mc.tooktook.objectives;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.tooktook.players.TeamType;

public class Objective {
	private Location location;
	private boolean isBroken;
	private TeamType type;
	
	public Objective(Location location, TeamType type){
		this.location = location;
		this.isBroken = false;
		this.type = type;
	}
	
	public boolean is(Block block){
		Logger.debug("checking objective at world "+location.getWorld().getName()+" , location "+Locations.printLocation(location));
		return block.getType().equals(Material.BEACON) && block.getLocation().getWorld().equals(location.getWorld()) && block.getLocation().distanceSquared(location) < 1;
	}
	
	public boolean canBreak(TeamType type){
		return !this.type.equals(type);
	}
	
	public boolean isBroken(){
		return isBroken;
	}
	
	public void setBroken(){
		this.isBroken = true;
	}
	
	public String toString(){
		return "[location='"+Locations.printLocation(location)+"', isBroken='"+isBroken+"']";
	}

	public Location getLocation() {
		return location;
	}

	public Object getType() {
		return type;
	}
	
	
}
