package com.gmail.val59000mc.tooktook.listeners;

import java.util.Calendar;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;


public class PlayerTeleportListener implements Listener{
	
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerTeleportWithEnderpearl(PlayerTeleportEvent event){
		handleTeleportEvent(event);
	}
	
	private void handleTeleportEvent(PlayerTeleportEvent event) {
		if(event.getCause().equals(TeleportCause.ENDER_PEARL)){
			

			Player player = event.getPlayer();
			TPlayer tPlayer = PlayersManager.instance().getTPlayer(player);
			
			if(tPlayer != null){
					
				Long now = Calendar.getInstance().getTimeInMillis();
				Long lastUse = tPlayer.getLastEnderpearl();
				if(now-lastUse < 15000){
					event.setCancelled(true);
					Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
					Logger.sendMessage(player, I18n.get("player.wait-enderpearl", player).replace("%time%",Time.getFormattedTime((15000-now+lastUse)/1000)));
					Inventories.give(player, new ItemStack(Material.ENDER_PEARL));
				}else{
					if(!Config.noLimit){
						tPlayer.setLastEnderpearl(now);
					}
					Sounds.playAll(event.getTo(), Sound.ENDERMAN_TELEPORT, 1, 1.2f);
				}
			}
			
			
		}
		
	}	
}
