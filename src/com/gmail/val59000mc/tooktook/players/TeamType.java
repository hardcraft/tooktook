package com.gmail.val59000mc.tooktook.players;

import org.bukkit.ChatColor;

public enum TeamType {
	RED(ChatColor.RED),
	BLUE(ChatColor.BLUE);
	
	private TeamType(ChatColor color) {
		this.color = color;
	}

	private ChatColor color;
	
	public ChatColor getColor() {
		return color;
	}
}
