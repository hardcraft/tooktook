package com.gmail.val59000mc.tooktook.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import com.gmail.val59000mc.tooktook.players.TPlayer;
import com.gmail.val59000mc.tooktook.players.TTeam;

public class PlayerClassManager {
	
	
	private static PlayerClassManager instance;
	private PlayerClass red;
	private PlayerClass blue;

	public static PlayerClassManager instance(){
		if(instance == null){
			instance = new PlayerClassManager();
		}
		
		return instance;
	}
	
	public PlayerClass getPlayerClass(TTeam team) {
		switch(team.getType()){
			case BLUE:
				return blue;
			default:
			case RED:
				return red;
		}
	}
	
	public static void load(){
		
		PlayerClassManager instance = PlayerClassManager.instance();

		// items
		List<ItemStack> items = new ArrayList<ItemStack>();
		items.add(addUnbreakable(new ItemStack(Material.WOOD_SWORD, 1)));
		items.add(addUnbreakable(new ItemStack(Material.WOOD_AXE, 1)));
		items.add(addUnbreakable(new ItemStack(Material.STONE_PICKAXE, 1)));
		items.add(new ItemStack(Material.APPLE, 1));
		items.add(new ItemStack(Material.COOKED_BEEF, 1));
		items.add(new ItemStack(Material.ENDER_PEARL, 2));
		items.add(new ItemStack(Material.ARROW, 2));
		

		// no limit items
		List<ItemStack> noLimitItems = new ArrayList<ItemStack>();
		noLimitItems.add(addUnbreakable(new ItemStack(Material.IRON_SWORD, 1)));
		noLimitItems.add(addUnbreakable(new ItemStack(Material.IRON_AXE, 1)));
		noLimitItems.add(addUnbreakable(new ItemStack(Material.IRON_PICKAXE, 1)));
		noLimitItems.add(new ItemStack(Material.APPLE, 1));
		noLimitItems.add(new ItemStack(Material.COOKED_BEEF, 5));
		noLimitItems.add(new ItemStack(Material.ENDER_PEARL, 8));
		noLimitItems.add(new ItemStack(Material.ARROW, 2));
		
		
		// Red
		List<ItemStack> redArmor = new ArrayList<ItemStack>();
		redArmor.add(addColor(addUnbreakable(new ItemStack(Material.LEATHER_HELMET, 1)),Color.fromRGB(255, 0, 0)));
		redArmor.add(addColor(addUnbreakable(new ItemStack(Material.LEATHER_CHESTPLATE, 1)),Color.fromRGB(255, 0, 0)));
		redArmor.add(new ItemStack(Material.AIR, 1));
		redArmor.add(new ItemStack(Material.AIR, 1));
		

		// no limit armor
		List<ItemStack> noLimitArmor = new ArrayList<ItemStack>();
		noLimitArmor.add(addUnbreakable(new ItemStack(Material.IRON_HELMET, 1)));
		noLimitArmor.add(addUnbreakable(new ItemStack(Material.IRON_CHESTPLATE, 1)));
		noLimitArmor.add(new ItemStack(Material.AIR, 1));
		noLimitArmor.add(new ItemStack(Material.AIR, 1));
		
		
		// Blue
		List<ItemStack> blueArmor = new ArrayList<ItemStack>();
		blueArmor.add(addColor(addUnbreakable(new ItemStack(Material.LEATHER_HELMET, 1)),Color.fromRGB(0,0,255)));
		blueArmor.add(addColor(addUnbreakable(new ItemStack(Material.LEATHER_CHESTPLATE, 1)),Color.fromRGB(0,0,255)));
		blueArmor.add(new ItemStack(Material.AIR, 1));
		blueArmor.add(new ItemStack(Material.AIR, 1));
		
		
		instance.red = new PlayerClass(PlayerClassType.RED,items,redArmor,noLimitItems, noLimitArmor);
		instance.blue = new PlayerClass(PlayerClassType.BLUE,items,blueArmor,noLimitItems, noLimitArmor);
	
	}
	
	private static ItemStack addUnbreakable(ItemStack stack){
		ItemMeta im = stack.getItemMeta();
		im.spigot().setUnbreakable(true);
		stack.setItemMeta(im);
		return stack;
	}
	
	private static ItemStack addColor(ItemStack stack, Color color){
		switch(stack.getType()){
			case LEATHER_HELMET:
			case LEATHER_CHESTPLATE:
			case LEATHER_LEGGINGS:
			case LEATHER_BOOTS:
				LeatherArmorMeta im = (LeatherArmorMeta) stack.getItemMeta();
				im.setColor(color);
				stack.setItemMeta(im);
				break;
			default:
				break;
		}
		return stack;
	}
	
	public void giveClassItems(TPlayer gPlayer) {
		if(gPlayer.getPlayerClass() != null){
			gPlayer.getPlayerClass().giveItemsAndArmor(gPlayer);
		}
	}
}
