package com.gmail.val59000mc.tooktook.dependencies;

import java.util.List;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.placeholders.SimplePlaceholder;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;
import com.gmail.val59000mc.tooktook.players.TTeam;
import com.gmail.val59000mc.tooktook.players.TeamType;
import com.google.common.collect.Lists;

public class TeamPlaceholder extends SimplePlaceholder{

	private TeamType type;
	
	public TeamPlaceholder(String pattern, TeamType type) {
		super(pattern);
		this.type = type;
	}
	
	@Override
	public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
		TTeam team = PlayersManager.instance().getTeam(type);
		List<TPlayer> members = team == null ? Lists.newArrayList() : team.getMembers();
		List<String> names = Lists.newArrayList();
		for(TPlayer teammate : members){
			names.add(team.getColor()+"- "+teammate.getName());
		}
		String replacement = String.join("<br>", names);
		return original.replaceAll(getPattern(), replacement);
	}

}
