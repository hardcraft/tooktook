package com.gmail.val59000mc.tooktook.players;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import ca.wacos.nametagedit.NametagAPI;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.tooktook.classes.PlayerClass;
import com.gmail.val59000mc.tooktook.classes.PlayerClassType;
import com.gmail.val59000mc.tooktook.dependencies.PermissionsExManager;
import com.gmail.val59000mc.tooktook.i18n.I18n;

public class TPlayer {
	private String name;
	private TTeam team;
	private PlayerState state;
	private boolean globalChat;
	private PlayerClass playerClass;
	private int kills;
	private int deaths;
	private double money;
	private Location spawnpoint;
	private double enderpearlsDropRate;
	private double arrowsDropRate;
	private long lastEnderpearl;	
	
	// Constructor
	
	public TPlayer(Player player){
		this.name = player.getName();
		this.team = null;
		this.state = PlayerState.WAITING;
		this.globalChat = true;
		this.playerClass = null;
		this.kills = 0;
		this.deaths = 0;
		this.money = 0;
		this.spawnpoint = null;
		this.enderpearlsDropRate = calculateDropRates(player,"enderpearl");			
		this.arrowsDropRate = calculateDropRates(player,"arrow");
		this.lastEnderpearl = 0;
		
	}
	
	
	
	// Accessors



	public String getName() {
		return name;
	}
	
	public long getLastEnderpearl() {
		return lastEnderpearl;
	}
	public void setLastEnderpearl(long lastEnderpearl) {
		this.lastEnderpearl = lastEnderpearl;
	}
	public double getEnderpearlsDropRate() {
		return enderpearlsDropRate;
	}
	public void setEnderpearlsDropRate(double enderpearlsDropRate) {
		this.enderpearlsDropRate = enderpearlsDropRate;
	}
	public double getArrowsDropRate() {
		return arrowsDropRate;
	}
	public void setArrowsDropRate(double arrowsDropRate) {
		this.arrowsDropRate = arrowsDropRate;
	}
	public Location getSpawnPoint() {
		return spawnpoint;
	}
	public void setSpawnPoint(Location location) {
		this.spawnpoint = location;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public synchronized TTeam getTeam() {
		return team;
	}
	public synchronized void setTeam(TTeam team) {
		this.team = team;
	}
	public PlayerState getState() {
		return state;
	}
	public void setState(PlayerState state) {
		this.state = state;
	}
	public double getMoney() {
		return money;
	}
	public void addMoney(double money) {
		this.money += money;
	}
	public boolean isGlobalChat() {
		return globalChat;
	}
	public void setGlobalChat(boolean globalChat) {
		this.globalChat = globalChat;
	}
	public PlayerClass getPlayerClass() {
		return playerClass;
	}
	public void setPlayerClass(PlayerClass playerClass) {
		this.playerClass = playerClass;
	}
	public int getDeaths() {
		return deaths;
	}
	public void addDeath() {
		this.deaths++;
	}
	public int getKills() {
		return kills;
	}
	public void addKill() {
		this.kills++;
		if(getTeam() != null){
			getTeam().addKill();
		}
	}
	public ChatColor getColor(){
		if(getTeam() != null){
			return getTeam().getColor();
		}else{
			return ChatColor.WHITE;
		}
	}
	
	// Methods


	public Player getPlayer(){
		return Bukkit.getPlayer(name);
	}
	
	public Boolean isOnline(){
		return Bukkit.getPlayer(name) != null;
	}
	
	public boolean isInTeamWith(TPlayer player){
		return (team != null && team.equals(player.getTeam()));
	}
	
	public void leaveTeam(){
		if(team != null){
			team.leave(this);
			team = null;
		}
	}

	public void sendI18nMessage(String code) {
		if(isOnline()){
			Logger.sendMessage(getPlayer(), I18n.get(code,getPlayer()));
		}
	}
	
	
	public String toString(){
		return "[name='"+getName()+
				"',team='"+((getTeam() == null) ? null : getTeam().getType())+"'"+
				",class='"+((getPlayerClass() == null) ? null : getPlayerClass().getType())+"']";
	}



	public boolean isClass(PlayerClassType type) {
		return getPlayerClass() != null && getPlayerClass().getType().equals(type);
	}



	public boolean isTeam(TeamType type) {
		return getTeam() != null && getTeam().getType().equals(type);
	}



	public void teleportToSpawnPoint() {
		if(isOnline() && getSpawnPoint() != null){
			getPlayer().teleport(getSpawnPoint());
		}
	}



	public boolean isState(PlayerState state) {
		return getState().equals(state);
	}



	public boolean isPlaying() {
		return isOnline() && isState(PlayerState.PLAYING);
	}


	
	private double calculateDropRates(Player player, String item) {

		double rate = 10;
		
		if(player.hasPermission("hardcraftpvp.tooktook."+item+".70")){
			rate = 70;
		}else if(player.hasPermission("hardcraftpvp.tooktook."+item+".60")){
			rate = 60;
		}else if(player.hasPermission("hardcraftpvp.tooktook."+item+".50")){
			rate = 50;
		}else if(player.hasPermission("hardcraftpvp.tooktook."+item+".40")){
			rate = 40;
		}else if(player.hasPermission("hardcraftpvp.tooktook."+item+".30")){
			rate = 30;
		}else if(player.hasPermission("hardcraftpvp.tooktook."+item+".20")){
			rate = 20;
		}
		
		return rate;
	}
	
	public void refreshNameTag(){
		String prefix = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', PermissionsExManager.getShortPrefix(getPlayer())));

		
		String suffix = "&f";
		
		if(getTeam() != null){
			switch(getTeam().getType()){
				case BLUE:
					suffix = "&9";
					break;
				case RED:
					suffix = "&c";
					break;
			}
			
		}
		
		NametagAPI.setPrefix(getName(), "&7"+prefix+suffix);
	}



}
