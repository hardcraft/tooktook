package com.gmail.val59000mc.tooktook.threads;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.tooktook.TookTook;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.players.PlayerState;
import com.gmail.val59000mc.tooktook.players.TPlayer;

public class UpdateScoreboardThread implements Runnable{
	

	private static UpdateScoreboardThread instance;
	private Map<TPlayer,SimpleScoreboard> scoreboards;
	


	public static void start(){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		Bukkit.getScheduler().runTaskAsynchronously(TookTook.getPlugin(), instance);
	}
	
	public static void add(TPlayer gPlayer){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		instance.addTPlayer(gPlayer);
	}
	
	private void addTPlayer(TPlayer gPlayer){
		if(!scoreboards.containsKey(gPlayer)){
			SimpleScoreboard sc = new SimpleScoreboard("Took-Took");
			if(gPlayer.isOnline()){
				setupTeamColors(sc,gPlayer);
			}
			getScoreboards().put(gPlayer, sc);
		}
	}
	
	private void setupTeamColors(SimpleScoreboard sc, TPlayer tPlayer) {
		
		Scoreboard scoreboard = sc.getBukkitScoreboard();
		
		Objective kills = scoreboard.registerNewObjective("kills", "playerKillCount");
		kills.setDisplaySlot(DisplaySlot.PLAYER_LIST);
	}
	
	private UpdateScoreboardThread(){
		this.scoreboards = Collections.synchronizedMap(new HashMap<TPlayer,SimpleScoreboard>());
	}
	
	private synchronized Map<TPlayer,SimpleScoreboard> getScoreboards(){
		return scoreboards;
	}
	
	public void run() {
		Bukkit.getScheduler().runTask(TookTook.getPlugin(), new Runnable(){

			public void run() {
					
				for(Entry<TPlayer,SimpleScoreboard> entry : getScoreboards().entrySet()){
					TPlayer evoPlayer = entry.getKey();
					SimpleScoreboard scoreboard = entry.getValue();
					
					if(evoPlayer.isOnline() && evoPlayer.getTeam() != null){
						updatePlayingScoreboard(evoPlayer,scoreboard);
					}else if(evoPlayer.isOnline()){
						updateSpectatingScoreboard(evoPlayer,scoreboard);
					}
				}

				Bukkit.getScheduler().runTaskLaterAsynchronously(TookTook.getPlugin(), instance, 20);
			}
			
			

			private void updateSpectatingScoreboard(TPlayer tPlayer,	SimpleScoreboard scoreboard) {

				Player player = tPlayer.getPlayer();
				
				List<String> content = new ArrayList<String>();
				content.add(" ");
				

				
				// No Limit Mode
				if(Config.noLimit){
					content.add(" ");
					content.add(I18n.get("game.no-limit-mode",player));
					
					content.add(" ");
					content.add(I18n.get("scoreboard.draw-in",player));
					content.add(" "+ChatColor.GREEN+Time.getFormattedTime(TimerAfterNoLimitThread.getRemainingTime()));
				}else{
					content.add(" ");
					content.add(I18n.get("scoreboard.no-limit-in",player));
					content.add(" "+ChatColor.GREEN+Time.getFormattedTime(NoLimitModeThread.getRemainingTime()));
				}
				
				scoreboard.clear();
				for(String line : content){
					scoreboard.add(line);
				}
				scoreboard.draw();
				scoreboard.send(player);
				
			}



			private void updatePlayingScoreboard(TPlayer tPlayer,	SimpleScoreboard scoreboard) {
				
				Player player = tPlayer.getPlayer();
				
				List<String> content = new ArrayList<String>();

				// Kills / Deaths
				content.add(I18n.get("scoreboard.kills-deaths",player));
				content.add(" "+ChatColor.GREEN+""+tPlayer.getKills()+ChatColor.WHITE+" / "+ChatColor.GREEN+tPlayer.getDeaths());
				
				// Coins
				content.add(I18n.get("scoreboard.coins-earned",player));
				content.add(" "+ChatColor.GREEN+""+tPlayer.getMoney());
				
				// Teammate
				content.add(I18n.get("scoreboard.teammates",player));
				for(TPlayer gTeamMate : tPlayer.getTeam().getMembers()){
					if(gTeamMate.isState(PlayerState.PLAYING)){
						content.add(" "+tPlayer.getColor()+""+gTeamMate.getName());
					}else{
						content.add(" "+ChatColor.GRAY+""+gTeamMate.getName());
					}
				}
				
				// Next Pearl
				Long now = Calendar.getInstance().getTimeInMillis();
				Long diff = now - tPlayer.getLastEnderpearl();
				if(diff < 15000){
					content.add(I18n.get("scoreboard.next-enderpearl",player)+"§r"+Time.getFormattedTime((15000-diff)/1000));
				}
				
				// No Limit Mode
				if(Config.noLimit){
					content.add(" ");
					content.add(I18n.get("game.no-limit-mode",player));

					content.add(I18n.get("scoreboard.draw-in",player));
					content.add(" "+ChatColor.GREEN+Time.getFormattedTime(TimerAfterNoLimitThread.getRemainingTime()));
				}
				
				scoreboard.clear();
				for(String line : content){
					scoreboard.add(line);
				}
				scoreboard.draw();
				scoreboard.send(player);
				
			}
				
			});
				
	}
	
	
	
	
}
