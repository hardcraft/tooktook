package com.gmail.val59000mc.tooktook.spawners;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class EntitySpawner extends Spawner{
	private EntityType type;
	
	public EntitySpawner(Location location, EntityType type, int limit, int time) {
		super(location, limit, time);
		this.type = type;
	}
	@Override
	public void spawn() {
		
		int count = 0;
		for(Entity entity : location.getWorld().getNearbyEntities(location, 3, 3, 3)){
			if(entity.getType().equals(type)){
				count++;
			}
		}
		
		if(count < limit){
			location.getWorld().spawnEntity(location, type);
		}
	}
	
	
}
