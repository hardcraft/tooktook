package com.gmail.val59000mc.tooktook.listeners;

import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.tooktook.game.GameManager;
import com.gmail.val59000mc.tooktook.game.GameState;
import com.gmail.val59000mc.tooktook.players.PlayerState;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;

public class PlayerDamageListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event){
		handleFriendlyFire(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event){
		handleAnyDamage(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPotionSplash(PotionSplashEvent event){
		handlePotionSplash(event);
	}
	

	///////////////////////
	// PotionSplashEvent //
	///////////////////////
	
	private void handlePotionSplash(PotionSplashEvent event) {
		if(event.getEntity().getShooter() instanceof Player){
			PlayersManager pm = PlayersManager.instance();
			
			Player damager = (Player) event.getEntity().getShooter();
			TPlayer tDamager = pm.getTPlayer(damager);
			
			if(tDamager != null){

				// Cancelling potion damage for teamates
				for(LivingEntity living : event.getAffectedEntities()){
					if(living instanceof Player){
						Player damaged = (Player) living;
						TPlayer tDamaged = pm.getTPlayer(damaged);
						if(tDamager.isInTeamWith(tDamaged)){
							event.setIntensity(living, 0);
						}
					}
				}
				
			}
			
		}
		
	}
	
	///////////////////////
	// EntityDamageEvent //
	///////////////////////

	private void handleAnyDamage(EntityDamageEvent event){
		if(event.getEntity() instanceof Player){
			
			if(!GameManager.instance().isState(GameState.PLAYING)){
				event.setCancelled(true);
			}else{
				if(event.getCause().equals(DamageCause.FALL)){
					event.setCancelled(true);
				}
				
				if(((Player) event.getEntity()).hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)){
					event.setCancelled(true);
				}
			}
			
			
		}
		
		
		
	}
	
	///////////////////////////////
	// EntityDamageByEntityEvent //
	///////////////////////////////
	
	private void handleFriendlyFire(EntityDamageByEntityEvent event){

		PlayersManager pm = PlayersManager.instance();
		
		// Direct ff hit
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			
			TPlayer evoDamager = pm.getTPlayer(damager);
			TPlayer evoDamaged = pm.getTPlayer(damaged);
			
			if(evoDamaged != null && evoDamager != null){
				if(evoDamaged.isInTeamWith(evoDamager)){
					event.setCancelled(true);
				}
			}
			
	    // Arrow ff hit
		}else if(event.getEntity() instanceof Player && event.getDamager() instanceof Projectile){
			Projectile projectile = (Projectile) event.getDamager();
			
			if(projectile instanceof EnderPearl){
				event.setCancelled(true);
				return;
			}
			
			final Player shot = (Player) event.getEntity();
			if(projectile.getShooter() instanceof Player){
								
				final Player shooter = (Player) projectile.getShooter();
				TPlayer evoDamager = pm.getTPlayer(shooter);
				TPlayer evoDamaged = pm.getTPlayer(shot);

				if(evoDamager != null && evoDamaged != null){
					if(evoDamager.getState().equals(PlayerState.PLAYING) && evoDamager.isInTeamWith(evoDamaged)){
						event.setCancelled(true);
					}
				}
			}

		}
		
	}
	
	
	

	
}
