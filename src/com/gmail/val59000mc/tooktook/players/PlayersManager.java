package com.gmail.val59000mc.tooktook.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.tooktook.classes.PlayerClassManager;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.dependencies.VaultManager;
import com.gmail.val59000mc.tooktook.game.EndCause;
import com.gmail.val59000mc.tooktook.game.GameManager;
import com.gmail.val59000mc.tooktook.game.GameState;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.tooktook.titles.TitleManager;

import net.md_5.bungee.api.ChatColor;

public class PlayersManager {
	private static PlayersManager instance;
	
	private List<TPlayer> players;
	private List<TTeam> teams;
	
	// static
	
	public static PlayersManager instance(){
		if(instance == null){
			instance = new PlayersManager();
		}
		
		return instance;
	}
	
	// constructor 
	
	private PlayersManager(){
		players = Collections.synchronizedList(new ArrayList<TPlayer>());
		teams = Collections.synchronizedList(new ArrayList<TTeam>());
		
		teams.add(new TTeam(TeamType.RED, Config.redSpawn));
		teams.add(new TTeam(TeamType.BLUE, Config.blueSpawn));
	}
	
	
	// Accessors
	
	public TPlayer getTPlayer(Player player){
		return getTPlayer(player.getName());
	}
	
	public TPlayer getTPlayer(String name){
		for(TPlayer tPlayer : getPlayers()){
			if(tPlayer.getName().equals(name))
				return tPlayer;
		}
		
		return null;
	}
	
	public synchronized List<TPlayer> getPlayers(){
		return players;
	}
	
	public synchronized List<TTeam> getTeams(){
		return teams;
	}


	public synchronized TTeam getTeam(TeamType type){
		for(TTeam team : getTeams()){
			if(team.getType().equals(type)){
				return team;
			}
		}
		return null;
	}

	public boolean canAddPlayerToTeam(TTeam evoTeam) {
		return evoTeam.getMembers().size() < getMinMembersTeam().getMembers().size()+1;
	}
	
	private TTeam getMinMembersTeam(){
		TTeam min = getTeams().get(0);
		for(TTeam team : getTeams()){
			if(team.getMembers().size() < min.getMembers().size()){
				min = team;
			}
		}
		return min;
	}
	
	public List<TTeam> getPlayingTeams() {

		List<TTeam> playingTeams = new ArrayList<TTeam>();

		for(TTeam slaveTeam : getTeams()){
			if(slaveTeam.isPlaying()){
				playingTeams.add(slaveTeam);
			}
		}
		
		return playingTeams;
	}
	
	
	// Methods 
	
	public synchronized TPlayer addPlayer(Player player){
		TPlayer newPlayer = new TPlayer(player);
		getPlayers().add(newPlayer);
		return newPlayer;
	}
	
	public synchronized void removePlayer(Player player){
		removePlayer(player.getName());
	}
	
	public synchronized void removePlayer(String name){
		TPlayer gPlayer = getTPlayer(name);
		if(gPlayer != null){
			gPlayer.leaveTeam();
			getPlayers().remove(gPlayer);
		}
	}
	
	public boolean isPlaying(Player player){
		TPlayer gPlayer = getTPlayer(player);
		if(gPlayer != null){
			return gPlayer.getState().equals(PlayerState.PLAYING);
		}
		return false;
	}

	public boolean isPlayerAllowedToJoin(Player player){
		Logger.debug("-> PlayersManager::isPlayerAllowedToJoin, player="+player.getName());
		GameManager gm = GameManager.instance();
		
		switch(gm.getState()){
				
			case WAITING:
			case PLAYING:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=true, gameState="+gm.getState());
				return true;
			case STARTING:
			case LOADING:
			case ENDED:
			default:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=false, gameState="+gm.getState());
				return false;
		}
	}

	public void playerJoinsTheGame(Player player) {
		Logger.debug("-> PlayersManager::playerJoinsTheGame, player="+player.getName());
		TPlayer gPlayer = getTPlayer(player);
		
		if(gPlayer == null){
			gPlayer = addPlayer(player);
		}
		
		GameState gameState = GameManager.instance().getState();
		switch(gameState){
			case WAITING:
				gPlayer.setState(PlayerState.WAITING);
				break;
			case STARTING:
			case LOADING:
			case ENDED:
				gPlayer.setState(PlayerState.DEAD);
				break;
			case PLAYING:
				if(!gPlayer.isState(PlayerState.PLAYING)){
					gPlayer.setState(PlayerState.DEAD);
				}
				break;
		}
			
		switch(gPlayer.getState()){
			case WAITING:
				Logger.debug("waitPlayer");
				gPlayer.sendI18nMessage("player.welcome");
				if(Config.isBountifulApiLoaded){
					TitleManager.sendTitle(player, ChatColor.GREEN+"Took-Took", 20, 20, 20);
				}
				Logger.broadcast(
						I18n.get("player.joined")
							.replace("%player%",gPlayer.getName())
							.replace("%count%", String.valueOf(getPlayers().size()))
							.replace("%total%",  String.valueOf(Config.maxPlayers))
					);
				waitPlayer(gPlayer);
				player.teleport(Config.lobby);
				break;
			case PLAYING:
				Logger.debug("relogPlayer");
				relogPlayer(gPlayer);
				break;
			case DEAD:
			default:
				Logger.debug("spectatePlayer");
				spectatePlayer(gPlayer);
				break;
		}
		
		gPlayer.refreshNameTag();
		refreshVisiblePlayers();
	}

	private void relogPlayer(TPlayer gPlayer) {
		if(gPlayer.isOnline() && GameManager.instance().isState(GameState.PLAYING) && gPlayer.getTeam() != null){
			Logger.broadcast(I18n.get("player.reconnect").replace("%player%", gPlayer.getName()));
		}else{
			spectatePlayer(gPlayer);
		}
	}

	public void waitPlayer(TPlayer gPlayer){
		
		if(gPlayer.isOnline()){
			if(Bukkit.getOnlinePlayers().size()>Config.maxPlayers){
				gPlayer.sendI18nMessage("player.full");
			}
			Player player = gPlayer.getPlayer();
			player.setGameMode(GameMode.ADVENTURE);
			Effects.addPermanent(player, PotionEffectType.SATURATION, 0);
			Effects.addPermanent(player, PotionEffectType.SPEED, 0);
			Effects.addPermanent(player, PotionEffectType.NIGHT_VISION, 0);
			player.setHealth(20);
			
			if(Config.isSIGLoaded){
				SIG.getPlugin().getAPI().giveInventoryToPlayer(player, "join");
			}
		}
		
	}
	
	public void startPlayer(final TPlayer tPlayer){
		tPlayer.setState(PlayerState.PLAYING);
		UpdateScoreboardThread.add(tPlayer);
		tPlayer.teleportToSpawnPoint();
		tPlayer.setGlobalChat(false);
		tPlayer.setGlobalChat(false);
		if(tPlayer.isOnline()){
			Player player = tPlayer.getPlayer();
			player.setFireTicks(0);
			Sounds.play(player, Sound.ENDERDRAGON_GROWL, 2, 2);
			Effects.clear(player);
		}
		teamSpawnPlayer(tPlayer);
	}
	


	public void teamSpawnPlayer(TPlayer evoPlayer) {
		if(evoPlayer.isOnline()){
			Player player = evoPlayer.getPlayer();
			player.setGameMode(GameMode.SURVIVAL);
			PlayerClassManager.instance().giveClassItems(evoPlayer);
		}
	}
	
	public void spectatePlayer(TPlayer evoPlayer){

		UpdateScoreboardThread.add(evoPlayer);
		evoPlayer.setState(PlayerState.DEAD);
		evoPlayer.sendI18nMessage("player.spectate");
		
		if(evoPlayer.isOnline()){
			Player player = evoPlayer.getPlayer();
			Inventories.clear(player);
			player.setGameMode(GameMode.SPECTATOR);
			player.teleport(Config.lobby);
		}
	}
	
	public List<TPlayer> getPlayingPlayers() {
		List<TPlayer> playingPlayers = new ArrayList<TPlayer>();
		for(TPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.PLAYING) && p.isOnline()){
				playingPlayers.add(p);
			}
		}
		return playingPlayers;
	}
	
	public List<TPlayer> getWaitingPlayers() {
		List<TPlayer> waitingPlayers = new ArrayList<TPlayer>();
		for(TPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.WAITING) && p.isOnline()){
				waitingPlayers.add(p);
			}
		}
		return waitingPlayers;
	}

	public void startAllPlayers() {
		Logger.debug("-> PlayersManager::startAllPlayers");
		assignRandomTeamsToPlayers();
		refreshVisiblePlayers();
		for(TPlayer gPlayer : getPlayers()){
			if(gPlayer.getTeam() == null){
				spectatePlayer(gPlayer);
			}else{
				startPlayer(gPlayer);
			}
		}
		UpdateScoreboardThread.start();
		Logger.debug("<- PlayersManager::startAllPlayers");
	}
	
	private void refreshVisiblePlayers() {
		for(Player player : Bukkit.getOnlinePlayers()){
			for(Player onePlayer : Bukkit.getOnlinePlayers()){
				player.hidePlayer(onePlayer);
				player.showPlayer(onePlayer);
			}
		}
	}
	
	private void assignRandomTeamsToPlayers(){
		Logger.debug("-> PlayersManager::assignRandomTeamsToPlayers");
		
		synchronized(players){
			// Shuffle players who will play (up to maxPlayers)
			List<TPlayer> playersWhoWillPlay = new ArrayList<TPlayer>(getWaitingPlayers().subList(0, Config.maxPlayers > players.size() ? players.size() : Config.maxPlayers));
			List<TPlayer> playersWhoWillNotPlay = new ArrayList<TPlayer>();
			if(players.size() > Config.maxPlayers){
				playersWhoWillNotPlay = new ArrayList<TPlayer>(getWaitingPlayers().subList(Config.maxPlayers, players.size()));	
			}
			this.players = new ArrayList<TPlayer>();
			Collections.shuffle(playersWhoWillPlay);
			this.players.addAll(playersWhoWillPlay);
			this.players.addAll(playersWhoWillNotPlay);
			
			for(TPlayer tPlayer : playersWhoWillNotPlay){
				tPlayer.leaveTeam();
			}
		}
		
		
		int nPlayer = 0;
		for(TPlayer evoPlayer : getWaitingPlayers()){
			nPlayer++;
			
			if(nPlayer <= Config.maxPlayers){
				
				TTeam team = evoPlayer.getTeam();
				
				if(evoPlayer.getTeam() == null){
					team = getMinMembersTeam();
					team.addPlayer(evoPlayer);
				}

				evoPlayer.setPlayerClass(PlayerClassManager.instance().getPlayerClass(evoPlayer.getTeam()));
				if(Config.isBountifulApiLoaded && evoPlayer.isOnline())
					TitleManager.sendTitle(evoPlayer.getPlayer(), I18n.get("player."+evoPlayer.getTeam().getType().toString().toLowerCase(),evoPlayer.getPlayer()), 10, 20 , 10);
				
			}
			
			evoPlayer.refreshNameTag();
		}

		Logger.debug("<- PlayersManager::assignRandomTeamsToPlayers");
	}

	public void endAllPlayers(EndCause cause, TTeam winningTeam) {
		
		if(winningTeam != null){
			for(TPlayer gPlayer : winningTeam.getMembers()){
				if(gPlayer.isOnline()){
					gPlayer.addMoney(VaultManager.addMoney(gPlayer.getPlayer(), Config.winReward));
				}
			}
		}

		for(TPlayer gPlayer : getPlayers()){
			gPlayer.setGlobalChat(true);
			if(gPlayer.isOnline()){
				Player player = gPlayer.getPlayer();
				spectatePlayer(gPlayer);
				printEndMessage(gPlayer,winningTeam);
				Logger.sendMessage(player, I18n.get("player.coins-earned",player)+gPlayer.getMoney());
			}
		}

		Sounds.playAll(Sound.ENDERDRAGON_GROWL,0.8f,2);
		
	}
	

	private void printEndMessage(TPlayer rPlayer, TTeam winningTeam){
		if(rPlayer.isOnline()){
			Player player = rPlayer.getPlayer();
			
			if(winningTeam == null){
				Texts.tellraw(player, I18n.get("stats.end.draw",player)
						.replace("%kills%", String.valueOf(rPlayer.getKills()))
						.replace("%deaths%", String.valueOf(rPlayer.getDeaths()))
						.replace("%hardcoins%", String.valueOf(rPlayer.getMoney()))
				);
			}else{
				Texts.tellraw(player, I18n.get("stats.end",player)
						.replace("%kills%", String.valueOf(rPlayer.getKills()))
						.replace("%deaths%", String.valueOf(rPlayer.getDeaths()))
						.replace("%hardcoins%", String.valueOf(rPlayer.getMoney()))
						.replace("%winner%", winningTeam.getColor()+winningTeam.getI18nName(player))
				);
			}
			
			
		}
	}
	
}
