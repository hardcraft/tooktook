package com.gmail.val59000mc.tooktook.dependencies;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.tooktook.game.GameManager;
import com.gmail.val59000mc.tooktook.game.GameState;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;
import com.gmail.val59000mc.tooktook.players.TTeam;
import com.gmail.val59000mc.tooktook.players.TeamType;

public class SelectTeamAction extends Action{
	
	private String team;
	
	public SelectTeamAction(String team){
		this.team = team;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {

		if(team.equals("leave")){
			leaveTeam(player,sigPlayer);
		}else{
			selectTeam(player,sigPlayer);
		}
		
		
	}

	private void selectTeam(Player player, SigPlayer sigPlayer) {
		TeamType type = TeamType.valueOf(team);
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			TPlayer evoPlayer = pm.getTPlayer(player);
			
			if(evoPlayer != null){
				TTeam oldTeam = evoPlayer.getTeam();
				evoPlayer.leaveTeam();
				
				TTeam newTeam = PlayersManager.instance().getTeam(type);
				
				if(newTeam.canAddPlayer()){
					newTeam.addPlayer(evoPlayer);
					evoPlayer.refreshNameTag();
					executeNextAction(player, sigPlayer, true);
					return;
				}else{
					if(oldTeam != null){
						oldTeam.addPlayer(evoPlayer);
					}
					evoPlayer.sendI18nMessage("team.full");
					Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
					executeNextAction(player, sigPlayer, false);
					return;
				}
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
	

	private void leaveTeam(Player player, SigPlayer sigPlayer) {
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			TPlayer evoPlayer = pm.getTPlayer(player);
			
			if(evoPlayer != null){
				evoPlayer.leaveTeam();
				evoPlayer.refreshNameTag();
				executeNextAction(player, sigPlayer, true);
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
}
