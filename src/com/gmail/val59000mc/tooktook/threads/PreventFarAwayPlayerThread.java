package com.gmail.val59000mc.tooktook.threads;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.tooktook.TookTook;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.game.GameManager;
import com.gmail.val59000mc.tooktook.game.GameState;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;

public class PreventFarAwayPlayerThread implements Runnable {
	
	private static PreventFarAwayPlayerThread thread;
	private boolean run;
	
	public static void start(){
		Logger.debug("-> KillFarAwayPlayerThread::start");
		PreventFarAwayPlayerThread thread = new PreventFarAwayPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(TookTook.getPlugin(), thread);
		Logger.debug("<- KillFarAwayPlayerThread::start");
	}
	
	private PreventFarAwayPlayerThread() {
		thread = this;
		this.run = true;
	}
	
	public static void stop() {
		thread.run = false;
	}

	public void run() {
		
		Bukkit.getScheduler().runTask(TookTook.getPlugin(), new Runnable(){

			public void run() {
				
				if(run){
					
					LocationBounds bounds = Config.bounds;
					
					checkObsidianOnBeacons();					
					
					if(bounds != null && bounds.getWorld() != null){

						World world = bounds.getWorld();
						
						for(Player player : Bukkit.getOnlinePlayers()){

							TPlayer wicPlayer = PlayersManager.instance().getTPlayer(player);
							
							if(wicPlayer != null && player.getWorld().equals(world)){
								if(!bounds.contains(player.getLocation())){
																		
									if( !GameManager.instance().isState(GameState.PLAYING) || player.getLocation().getY() >= 0){
										if(wicPlayer.isPlaying() && wicPlayer.getTeam() != null){
											player.teleport(wicPlayer.getSpawnPoint());
										}else{
											player.teleport(Config.lobby);
										}
										wicPlayer.sendI18nMessage("game.leave-arena");
									}else if(GameManager.instance().isState(GameState.PLAYING) 
											&& wicPlayer.isPlaying()
											&& player.getLocation().getY() < 0 
											&& player.getHealth() > 0){
										player.setHealth(0);
										Sounds.play(player, Sound.HURT_FLESH, 1, 1);
									}
									
								}
							}
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(TookTook.getPlugin(), thread, 30);
				}
				
				
			}

			private void checkObsidianOnBeacons() {
				
				Block redBeacon = Config.redBeacon.getBlock();
				Block blueBeacon = Config.blueBeacon.getBlock();
				
				removeObsidian(redBeacon.getRelative(BlockFace.UP));
				removeObsidian(redBeacon.getRelative(BlockFace.DOWN));
				removeObsidian(redBeacon.getRelative(BlockFace.EAST));
				removeObsidian(redBeacon.getRelative(BlockFace.WEST));
				removeObsidian(redBeacon.getRelative(BlockFace.NORTH));
				removeObsidian(redBeacon.getRelative(BlockFace.SOUTH));
				
				removeObsidian(blueBeacon.getRelative(BlockFace.UP));
				removeObsidian(blueBeacon.getRelative(BlockFace.DOWN));
				removeObsidian(blueBeacon.getRelative(BlockFace.EAST));
				removeObsidian(blueBeacon.getRelative(BlockFace.WEST));
				removeObsidian(blueBeacon.getRelative(BlockFace.NORTH));
				removeObsidian(blueBeacon.getRelative(BlockFace.SOUTH));
				
			}
			
			private void removeObsidian(Block block) {
				
				if(block.getType().equals(Material.OBSIDIAN)){
					block.setType(Material.COBBLESTONE);
				}
				
			}
			
		});
		
	}

}
