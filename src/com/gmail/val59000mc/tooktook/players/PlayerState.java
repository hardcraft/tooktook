package com.gmail.val59000mc.tooktook.players;

public enum PlayerState {
  WAITING,
  PLAYING,
  DEAD;
}
