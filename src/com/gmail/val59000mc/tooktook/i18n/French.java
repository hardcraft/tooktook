package com.gmail.val59000mc.tooktook.i18n;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;

public class French {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();
		
		// config
		s.put("config.dependency.vault-not-found", "Le plugin Vault est introuvable, pas de support pour les recompenses");
		s.put("config.dependency.vault-loaded", "Le plugin Vault a ete correctement charge.");
		s.put("config.dependency.worldedit-not-found", "Le plugin WorldEdit est introuvable, pas de support pour les schematics");
		s.put("config.dependency.worldedit-loaded", "Le plugin WorldEdit a ete correctement charge.");
		s.put("config.dependency.sig-not-found", "Le plugin SimpleInventoryGUI est introuvable, pas de support pour les shops");
		s.put("config.dependency.sig-loaded", "Le plugin SimpleInventoryGUI a ete correctement charge.");
		s.put("config.dependency.bountifulapi-not-found", "Le plugin BountifulAPI est introuvable, pas de support pour les titres");
		s.put("config.dependency.bountifulapi-loaded", "Le plugin BountifulAPI a ete correctement charge.");
		
		// commands
		s.put("command.global-chat.true", "§aTu parles désormais à tout le monde.");
		s.put("command.global-chat.false", "§aTu parles désormais à ta team.");
		s.put("command.start.not-possible", "§cTu ne peux pas forcer le démarrage du jeu.");
		s.put("command.start.ok", "§aLe lancement du jeu a été forcé.");
		
		// parser
		s.put("parser.wrong-location", "Impossible de parser la location %location%");
		
		s.put("parser.item.not-found", "Texte de config l'item non trouve");
		s.put("parser.item.empty-string", "Texte de config l'item vide");
		s.put("parser.item.wrong-material", "La materiau n'existe pas");
		s.put("parser.item.wrong-damage-value", "Erreur de damage value");
		s.put("parser.item.wrong-amount", "Erreur sur la quantite");
		s.put("parser.item.wrong-enchantment-syntax", "Mauvaise syntaxe d'enchantement");
		s.put("parser.item.wrong-enchantment-name", "Mauvais nom d'enchantement");
		s.put("parser.item.wrong-enchantment-level", "Mauvais niveau d'enchantement");
		
		s.put("parser.playerclass.not-found", "Classe de joueur introuvable");
		s.put("parser.playerclass.items-not-found", "items introuvables dans la classe de joueur");
		s.put("parser.playerclass.wrong-player-class", "impossible de creer la classe de joueur");
		
					
		// map loader
		s.put("map-loader.load.no-last-world", "Pas d'ancienne map a charger, creation d'une nouvelle map");
		s.put("map-loader.load.last-world-not-found", "Ancienne map non trouvee, creation d'une nouvelle map");
		s.put("map-loader.delete.no-last-world", "Pas d'ancienne map a supprimer.");
		s.put("map-loader.delete.last-world-not-found", "Ancienne map a supprimer non trouvee.");
		s.put("map-loader.delete", "Suppression de l'ancienne map.");
		s.put("map-loader.copy.not-found", "Dossier 'winteriscoming' introuvable, copie impossible.");
		
		// game
		s.put("game.player-allowed-to-join", "Les joueurs peuvent maintenant rejoindre la partie");
		s.put("game.start", "La partie commence !");
		s.put("game.starting-in", "La partie va commencer dans %time% !");
		s.put("game.end-in", "La partie va finir dans %time car il n'y a plus assez de joueurs.");
		s.put("game.end", "La partie est terminee !");
		s.put("game.end-stopped", "La partie continue !");
		s.put("game.shutting-down-in", "Arret dans %time% !");
		s.put("game.remaining-time", "Il reste %time% !");
		s.put("game.end.draw-in", ChatColor.GREEN+"Match nul dans %time%.");
		s.put("game.end.draw", ChatColor.GREEN+"La partie est finie ! Match Nul !");
		s.put("game.end.team-win", ChatColor.GREEN+"La partie est finie ! Equipe gagnante : ");
		s.put("game.end.no-more-players", ChatColor.GREEN+"La partie est finie car il n'y a plus de joueurs.");
		s.put("game.leave-arena", ChatColor.RED+"N'essaie pas de sortir de la map.");
		s.put("game.no-limit-mode", ChatColor.GREEN+"Mode No Limit !");
		s.put("game.no-limit-mode-in", ChatColor.GREEN+"Mode No Limit activé dans %time%.");

		// players
		s.put("player.not-allowed-to-join", "Vous n'etes pas autorise a rejoindre cette partie.");
		s.put("player.welcome", "Bienvenue dans le jeu Took-Took !");
		s.put("player.joined", "§f%player% §aa rejoint la partie §2§l[%count%/%total%]");
		s.put("player.full", "La partie est pleine. Si personne ne se deconnecte, tu seras spectateur.");
		s.put("player.spectate", "Vous etes spectateur.");
		s.put("player.red", "§cEquipe rouge !");
		s.put("player.blue", "§1Equipe bleue !");
		s.put("player.coins-earned", ChatColor.GREEN+"HardCoins gagnés : ");
		s.put("player.ennemy-spawn-forbidden", ChatColor.RED+"Tu ne peux pas attaquer le spawn adverse");
		s.put("player.died", "%player% §rest mort");
		s.put("player.killed", "%killer% §ra tué %killed%");
		s.put("player.wait-enderpearl", "§cAttends %time% avant d'utiliser une autre perle.");
		s.put("player.kill-after-disconnect-warning", "§7%player% s'est déconnecté. Il a jusque %time% pour se reconnecter.");
		s.put("player.kill-after-disconnect", "§7%player% ne s'est pas reconnecté et a été éliminé.");
		s.put("player.reconnect", "§7%player% s'est reconnecté.");
		s.put("player.bed-destroyed", "§cTon li a été détruit !");
		s.put("player.eliminated", "%player% §6a été éliminé !");

		s.put("stats.end", "[\"\",{text:\"]----[ Résumé de la partie de Took-Took ]----[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Tués : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Morts : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins gagnés : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"  >>  Equipe gagnante : \",bold:true,color:green},{text:\"%winner%\n\n\"},{text:\"]----------------------------------------[\",color:green}]");
		s.put("stats.end.draw", "[\"\",{text:\"]----[ Résumé de la partie de Took-Took ]----[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Tués : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Morts : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins gagnés : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"  >>  Equipe gagnante : \",bold:true,color:green},{text:\"Match nul\n\n\"},{text:\"]----------------------------------------[\",color:green}]");
		
		// team
		s.put("team.RED", "Rouge");
		s.put("team.BLUE", "Bleu");
		s.put("team.full", "§cIl y a déjà trop de joueurs dans cette équipe.");
		
		// scoreboard
		s.put("scoreboard.team", "Equipe");
		s.put("scoreboard.kills-deaths", "Tués/Morts");
		s.put("scoreboard.player-class", "Classe");
		s.put("scoreboard.coins-earned", "Coins");
		s.put("scoreboard.teammates", "Coéquipiers");
		s.put("scoreboard.online-players", "Joueurs");
		s.put("scoreboard.time-to-start", "Commence dans");
		s.put("scoreboard.next-enderpearl", "§cProchaine perle ");
		s.put("scoreboard.draw-in", "Match nul");
		s.put("scoreboard.no-limit-in", "Mode No Limit");
		
		// objectives
		s.put("objectives.cannot-break-team-grief", ChatColor.RED+"Seuls les ennemis peuvent casser l'objectif.");
		s.put("objectives.broken-by", ChatColor.YELLOW+"%player% a casse l'objectif");
		
		// ping
		s.put("ping.loading", "Chargement");
		s.put("ping.playing", "En jeu");
		s.put("ping.starting", "Commence");
		s.put("ping.waiting", "En attente");
		s.put("ping.ended", "Fin");
		
		return s;
	}

}
