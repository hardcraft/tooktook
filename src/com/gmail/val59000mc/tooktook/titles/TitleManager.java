package com.gmail.val59000mc.tooktook.titles;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.connorlinfoot.bountifulapi.BountifulAPI;

public class TitleManager {
	
	public static void sendTitle(Player player, String message, int fadeIn, int stay, int fadeOut){
		BountifulAPI.sendTitle(player, fadeIn, stay, fadeOut, message, "");
	}
	
	public static void sendAllTitle(String message, int fadeIn, int stay, int fadeOut){
		for(Player player : Bukkit.getOnlinePlayers()){
			BountifulAPI.sendTitle(player, fadeIn, stay, fadeOut, message, "");
		}
	}
}
