package com.gmail.val59000mc.tooktook.listeners;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.dependencies.VaultManager;
import com.gmail.val59000mc.tooktook.game.EndCause;
import com.gmail.val59000mc.tooktook.game.GameManager;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.objectives.Objective;
import com.gmail.val59000mc.tooktook.objectives.ObjectivesManager;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TPlayer;

public class BlockListener implements Listener{
	
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockBreak(final BlockBreakEvent event){
		TPlayer tPlayer = PlayersManager.instance().getTPlayer(event.getPlayer());
		
		if(tPlayer == null){
			event.setCancelled(true);
			return;
		}
		
		handleBreakObjective(event,tPlayer);
		handleBreakIronORGold(event,tPlayer);
	}
	
	private void handleBreakIronORGold(BlockBreakEvent event, TPlayer tPlayer) {
		if(!event.isCancelled()){
			
			Block block = event.getBlock();
			World world = block.getWorld();
			switch(block.getType()){
				case IRON_ORE:
					event.setCancelled(true);
					world.dropItem(block.getLocation().clone().add(0.5,0.5,0.5), new ItemStack(Material.IRON_INGOT));
					block.setType(Material.AIR);
					break;
				case GOLD_ORE:
					event.setCancelled(true);
					world.dropItem(block.getLocation().clone().add(0.5,0.5,0.5), new ItemStack(Material.GOLD_INGOT));
					block.setType(Material.AIR);
					break;
				default:
					break;
			}
		}
	}

	private void handleBreakObjective(final BlockBreakEvent event,TPlayer tPlayer){
		Logger.debug("-> BlockListener::handleBreakObjective, player="+tPlayer.getName());
		if(!event.isCancelled()){
			ObjectivesManager om = ObjectivesManager.instance();
			Objective objective = om.getObjective(event.getBlock());
			
			if(objective != null){
				Logger.debug("objective not null");
				
				boolean broken = om.tryBreakObjective(objective, tPlayer);
				event.setCancelled(true);
				
				if(broken){
					if(tPlayer.isOnline()){
						tPlayer.addMoney(rewardBreak(tPlayer.getPlayer()));
					}
					event.getBlock().setType(Material.AIR);

					Logger.broadcast(I18n.get("objectives.broken-by").replace("%player%", tPlayer.getName()));
					GameManager.instance().endGame(EndCause.TEAM_WIN, tPlayer.getTeam());
				}
				
			}
		}
		Logger.debug("<- BlockListener::handleBreakObjective");
	}
	
	private double rewardBreak(Player breaker) {
		Logger.debug("-> BlockListener::rewardBreak, player="+breaker.getName());
		double reward = 0;
		if(Config.isVaultLoaded){
			reward = VaultManager.addMoney(breaker, Config.objectiveReward);
			breaker.sendMessage(ChatColor.GREEN+"+"+reward+" "+ChatColor.WHITE+"HC");
			Sounds.play(breaker, Sound.LEVEL_UP, 0.5f, 2f);
		}
		Logger.debug("<- BlockListener::rewardBreak");
		return reward;
	}
}
