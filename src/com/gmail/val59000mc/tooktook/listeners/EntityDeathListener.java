package com.gmail.val59000mc.tooktook.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

public class EntityDeathListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDeath(EntityDeathEvent event) {

		handleCookedFoodDrop(event);
		
	}
	
	private void handleCookedFoodDrop(EntityDeathEvent event){
			for(int i=0 ; i<event.getDrops().size() ; i++){
				Material replaceBy = null;
				switch(event.getDrops().get(i).getType()){
					case RAW_BEEF:
						replaceBy = Material.COOKED_BEEF;
						break;
					case RAW_CHICKEN:
						replaceBy = Material.COOKED_CHICKEN;
						break;
					case MUTTON:
						replaceBy = Material.COOKED_MUTTON;
						break;
					case RABBIT:
						replaceBy = Material.COOKED_RABBIT;
						break;
					case PORK:
						replaceBy = Material.GRILLED_PORK;
						break;
					default:
						break;
				}
				if(replaceBy != null){
					ItemStack cookedFood = event.getDrops().get(i).clone();
					cookedFood.setType(replaceBy);
					event.getDrops().set(i, cookedFood);
				}
			}
	}
}
