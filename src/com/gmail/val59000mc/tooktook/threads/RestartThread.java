package com.gmail.val59000mc.tooktook.threads;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Bungee;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.tooktook.TookTook;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.i18n.I18n;

public class RestartThread implements Runnable{

	private static RestartThread instance;
	
	RestartThread thread;
	long remainingTime;
	
	
	public static void start(){
		Logger.debug("-> RestartThread::start");
		if(instance == null){
			Bukkit.getScheduler().runTaskAsynchronously(TookTook.getPlugin(), new RestartThread());
		}
		Logger.debug("<- RestartThread::start");
	}
	
	
	public RestartThread(){
		instance = this;
		this.remainingTime = 15;
	}
	
	public void run(){
		
		Bukkit.getScheduler().runTask(TookTook.getPlugin(), new Runnable() {
			
			public void run() {
				remainingTime--;
				
				if(remainingTime == 5){
					if(Config.isBungeeEnabled){
						for(Player player : Bukkit.getOnlinePlayers()){
							Bungee.sendPlayerToServer(TookTook.getPlugin(),player,Config.bungeeServer);
						}
					}
				}
				
				if(remainingTime <= 0){
					
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-all");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
				}else{
					Bukkit.getScheduler().runTaskLaterAsynchronously(TookTook.getPlugin(), instance, 20);
				}
			}
		});		
		
	}

}
