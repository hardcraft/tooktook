package com.gmail.val59000mc.tooktook.classes;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.players.TPlayer;

public class PlayerClass {
	private PlayerClassType type;
	protected List<ItemStack> items;
	protected List<ItemStack> armor;
	protected List<ItemStack> noLimitItems;
	protected List<ItemStack> noLimitArmor;
	
	public PlayerClass(PlayerClassType type, 
			List<ItemStack> items, 
			List<ItemStack> armor,
			List<ItemStack> noLimitItems,
			List<ItemStack> noLimitArmor) {
		super();
		this.type = type;
		this.items = items;
		this.armor = armor;
		this.noLimitItems = noLimitItems;
		this.noLimitArmor = noLimitArmor;
	}

	public PlayerClassType getType() {
		return type;
	}
	
	public void giveItemsAndArmor(TPlayer tPlayer){
		if(tPlayer.isOnline()){
			Player player = tPlayer.getPlayer();
			Inventories.clear(player);
			Inventories.setArmor(player, (Config.noLimit ? noLimitArmor : armor));
			Inventories.give(player, (Config.noLimit ? noLimitItems : items));
			Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 50,  200);
			if(Config.noLimit){
				Effects.addPermanent(player, PotionEffectType.NIGHT_VISION, 0);
				Effects.addPermanent(player, PotionEffectType.SPEED, 0);
			}
		}
	}
	

	
	public void giveClassItems(TPlayer gPlayer) {
		if(gPlayer.getPlayerClass() != null){
			gPlayer.getPlayerClass().giveItemsAndArmor(gPlayer);
		}
	}
	
}
