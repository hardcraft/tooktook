package com.gmail.val59000mc.tooktook;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.tooktook.game.GameManager;

public class TookTook extends JavaPlugin{
	
	private static TookTook pl;
	
	
	public void onEnable(){
		pl = this;
	
		// Blocks players joins while loading the plugin
		Bukkit.getServer().setWhitelist(true);
		saveDefaultConfig();
		
		Logger.setColoredPrefix(ChatColor.WHITE+"["+ChatColor.GREEN+"Took-Took"+ChatColor.WHITE+"]"+ChatColor.RESET+" ");
		Logger.setStrippedPrefix("[Took-Took] ");
		
		Bukkit.getScheduler().runTaskLater(this, new Runnable(){
			
			public void run() {
				GameManager.instance().loadGame();
				
				// Unlock players joins and rely on UhcPlayerJoinListener
				Bukkit.getServer().setWhitelist(false);
			}
			
		}, 1);
		
		
	}
	
	public static TookTook getPlugin(){
		return pl;
	}
	
	public void onDisable(){
	}
}
