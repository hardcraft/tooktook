package com.gmail.val59000mc.tooktook.game;

public enum EndCause {
	TEAM_WIN("game.end.team-win"),
	DRAW("game.end.draw"),
	NO_MORE_PLAYERS("game.end.no-more-players");
	
	private String code;

	private EndCause(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	
	
}
