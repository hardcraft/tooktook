package com.gmail.val59000mc.tooktook.spawners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

public class ItemSpawner extends Spawner{
	private Material type;
	
	public ItemSpawner(Location location, Material type, int limit, int time) {
		super(location, limit, time);
		this.type = type;
	}
	
	@Override
	public void spawn() {
		
		int count = 0;
		for(Entity entity : location.getWorld().getNearbyEntities(location, 3, 3, 3)){
			if(entity.getType().equals(EntityType.DROPPED_ITEM)){
				ItemStack stack = ((Item) entity).getItemStack();
				if(stack.getType().equals(type)){
					count += stack.getAmount();
				}
			}
		}
		
		if(count < limit){
			location.getWorld().dropItem(location, new ItemStack(type));
		}
	}
	
	
}
