package com.gmail.val59000mc.tooktook.objectives;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.players.TPlayer;
import com.gmail.val59000mc.tooktook.players.TeamType;

public class ObjectivesManager {
	
	private static ObjectivesManager instance;
	
	private List<Objective> objectives;
	
	public static ObjectivesManager instance(){
		if(instance == null){
			instance = new ObjectivesManager();
		}
		
		return instance;
	}
	
	public synchronized List<Objective> getObjectives() {
		return objectives;
	}

	private ObjectivesManager(){
		objectives = Collections.synchronizedList(new ArrayList<Objective>());
	}
	
	public static void load(){
		Logger.debug("-> ObjectivesManager::load");
		ObjectivesManager.instance();
		instance.objectives.add(new Objective(Config.redBeacon,TeamType.RED));
		instance.objectives.add(new Objective(Config.blueBeacon,TeamType.BLUE));
		Logger.debug("<- ObjectivesManager::load");
	}

	
	public Objective getObjective(Block block){
		Logger.debug("-> ObjectivesManager:: getObjective, block="+block.getType().toString());
		if(block == null || !block.getType().equals(Material.BEACON)){
			Logger.debug("block is not a beacon");
			return null;
		}
		
		Logger.debug("objectives, size="+getObjectives().size());
		
		for(Objective obj : getObjectives()){
			if(obj.is(block)){
				return obj;
			}
		}
		return null;
	}
	
	public boolean tryBreakObjective(Objective objective, TPlayer tPlayer){
		Logger.debug("-> ObjectivesManager:: canBreakObjective, objective="+objective+", tPlayer="+tPlayer);
		
		if(objective == null || tPlayer == null || tPlayer.getTeam() == null || !tPlayer.isOnline() || (objective != null && objective.isBroken())){
			Logger.debug("<- ObjectivesManager:: canBreakObjective, canBreak=false");
			return false;
		}
		
		if(!objective.canBreak(tPlayer.getTeam().getType())){
			tPlayer.sendI18nMessage("objectives.cannot-break-team-grief");
			Logger.debug("<- ObjectivesManager:: canBreakObjective, canBreak=false");
			return false;
		}
		
		doBreakObjective(objective, tPlayer);		
			
		Logger.debug("<- ObjectivesManager:: canBreakObjective, canBreak=true");
		
		return true;
	}
	
	private void doBreakObjective(Objective objective, TPlayer tPlayer){

		Sounds.playAll(Sound.ZOMBIE_WOODBREAK, 1.5f, 0.5f);
		objective.setBroken();
	}
}
