package com.gmail.val59000mc.tooktook.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.tooktook.TookTook;
import com.gmail.val59000mc.tooktook.classes.PlayerClassManager;
import com.gmail.val59000mc.tooktook.commands.ChatCommand;
import com.gmail.val59000mc.tooktook.commands.StartCommand;
import com.gmail.val59000mc.tooktook.configuration.Config;
import com.gmail.val59000mc.tooktook.i18n.I18n;
import com.gmail.val59000mc.tooktook.listeners.BlockListener;
import com.gmail.val59000mc.tooktook.listeners.EntityDeathListener;
import com.gmail.val59000mc.tooktook.listeners.PingListener;
import com.gmail.val59000mc.tooktook.listeners.PlayerChatListener;
import com.gmail.val59000mc.tooktook.listeners.PlayerConnectionListener;
import com.gmail.val59000mc.tooktook.listeners.PlayerDamageListener;
import com.gmail.val59000mc.tooktook.listeners.PlayerDeathListener;
import com.gmail.val59000mc.tooktook.listeners.PlayerTeleportListener;
import com.gmail.val59000mc.tooktook.maploader.MapLoader;
import com.gmail.val59000mc.tooktook.objectives.ObjectivesManager;
import com.gmail.val59000mc.tooktook.players.PlayersManager;
import com.gmail.val59000mc.tooktook.players.TTeam;
import com.gmail.val59000mc.tooktook.threads.CheckRemainingPlayerThread;
import com.gmail.val59000mc.tooktook.threads.NoLimitModeThread;
import com.gmail.val59000mc.tooktook.threads.PreventFarAwayPlayerThread;
import com.gmail.val59000mc.tooktook.threads.RestartThread;
import com.gmail.val59000mc.tooktook.threads.SpawnerThread;
import com.gmail.val59000mc.tooktook.threads.TimerAfterNoLimitThread;
import com.gmail.val59000mc.tooktook.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.tooktook.threads.WaitForNewPlayersThread;
import com.gmail.val59000mc.tooktook.titles.TitleManager;


public class GameManager {

	private static GameManager instance = null;
	
	private GameState state;
	
	// static
	
	public static GameManager instance(){
		if(instance == null){
			instance = new GameManager();
		}
		return instance;
	}

	
	// constructor 
	
	private GameManager(){
		this.state = GameState.LOADING;
	}

	
	// accessors
	
	public GameState getState() {
		return state;
	}


	public boolean isState(GameState state) {
		return getState().equals(state);
	}
	
	// methods 
	
	public void loadGame() {
		state = GameState.LOADING;
		Logger.debug("-> GameManager::loadNewGame");
		Config.load();
		
		MapLoader.deleteOldPlayersFiles();
		MapLoader.load();
		
		PlayerClassManager.load();
		ObjectivesManager.load();
		
		registerCommands();
		registerListeners();
		
		if(Config.isBungeeEnabled)
			TookTook.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(TookTook.getPlugin(), "BungeeCord");

		waitForNewPlayers();
		Logger.debug("<- GameManager::loadNewGame");
	}


	
	private void registerListeners(){
		Logger.debug("-> GameManager::registerListeners");
		// Registers Listeners
		List<Listener> listeners = new ArrayList<Listener>();		
		listeners.add(new PlayerConnectionListener());	
		listeners.add(new PlayerChatListener());
		listeners.add(new PlayerDamageListener());
		listeners.add(new PlayerDeathListener());
		listeners.add(new PingListener());
		listeners.add(new BlockListener());
		//listeners.add(new ItemListener());
		listeners.add(new PlayerTeleportListener());
		listeners.add(new EntityDeathListener());
		for(Listener listener : listeners){
			Logger.debug("Registering listener="+listener.getClass().getSimpleName());
			Bukkit.getServer().getPluginManager().registerEvents(listener, TookTook.getPlugin());
		}
		Logger.debug("<- GameManager::registerListeners");
	}
	
	private void registerCommands(){
		Logger.debug("-> GameManager::registerCommands");
		// Registers Listeners	
		TookTook.getPlugin().getCommand("chat").setExecutor(new ChatCommand());
		TookTook.getPlugin().getCommand("start").setExecutor(new StartCommand());
		Logger.debug("<- GameManager::registerCommands");
	}



	private void waitForNewPlayers(){
		Logger.debug("-> GameManager::waitForNewPlayers");
		WaitForNewPlayersThread.start();
		PreventFarAwayPlayerThread.start();
		UpdateScoreboardThread.start();
		state = GameState.WAITING;
		Logger.infoC(I18n.get("game.player-allowed-to-join"));
		Logger.debug("<- GameManager::waitForNewPlayers");
	}
	
	public void startGame(){
		Logger.debug("-> GameManager::startGame");
		state = GameState.STARTING;
		Logger.broadcast(I18n.get("game.start"));
		PlayersManager.instance().startAllPlayers();	
		playGame();
		Logger.debug("<- GameManager::startGame");
	}
	
	private void playGame(){
		state = GameState.PLAYING;
		CheckRemainingPlayerThread.start();
		SpawnerThread.start(Config.spawners);
		NoLimitModeThread.start();
	}
	
	public void endGame(EndCause cause, TTeam winningTeam) {
		if(state.equals(GameState.PLAYING)){
			state = GameState.ENDED;
			NoLimitModeThread.stop();
			TimerAfterNoLimitThread.stop();
			CheckRemainingPlayerThread.stop();
			PreventFarAwayPlayerThread.stop();
			SpawnerThread.stop();
			PlayersManager.instance().endAllPlayers(cause,winningTeam);
			RestartThread.start();
		}
		
	}


	public void startNoLimitMode() {
		Config.noLimit = true;
		Logger.broadcast(I18n.get("game.no-limit-mode"));
		Sounds.playAll(Sound.ENDERMAN_SCREAM);
		TimerAfterNoLimitThread.start();
		if(Config.isBountifulApiLoaded){
			TitleManager.sendAllTitle(I18n.get("game.no-limit-mode"), 20, 20, 20);
		}
		for(Player player : Bukkit.getOnlinePlayers()){
			Effects.addPermanent(player, PotionEffectType.NIGHT_VISION, 0);
			Effects.addPermanent(player, PotionEffectType.SPEED, 0);
			Inventories.give(player, new ItemStack(Material.ENDER_PEARL, 8));
		}
	}

	
	


	
	
	

}
